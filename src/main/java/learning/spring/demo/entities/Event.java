package learning.spring.demo.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import learning.spring.demo.common.Status;
import lombok.Data;

import java.util.Date;

@Data
@Entity
@Table(name = "Events")
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name", nullable = false, columnDefinition = "varchar", length = 50)
    private String name;

    @Column(name = "description", columnDefinition = "varchar")
    private String description;

    @JsonFormat(pattern = "dd/mm/yyyy")
    @Column(name = "start_date", nullable = false)
    private Date startDate;

    @JsonFormat(pattern = "dd/mm/yyyy")
    @Column(name = "end_date", nullable = false)
    private Date endDate;

    @Enumerated(EnumType.STRING)
    @Column(name="status")
    private Status status = Status.DRAFT;

    @Column(name="user_id", nullable = false, columnDefinition = "bigint", length = 20)
    private long userId;

    @Column(name="org_id", columnDefinition = "bigint", length = 20)
    private  long orgId;

    @Column(name="deleted", columnDefinition = "boolean")
    private  boolean deleted = false;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd/mm/yyyy h:m:s")
    @Column(name = "created_at", nullable = false, updatable = false)
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd/mm/yyyy h:m:s")
    @Column(name = "updated_at")
    private Date updatedAt;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd/mm/yyyy h:m:s")
    @Column(name = "deleted_at")
    private Date deletedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getOrgId() {
        return orgId;
    }

    public void setOrgId(long orgId) {
        this.orgId = orgId;
    }

    @PrePersist
    protected void onCreate() {
        createdAt = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updatedAt = new Date();
    }

    protected void markAsDeleted() {
        this.deletedAt = new Date();
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
