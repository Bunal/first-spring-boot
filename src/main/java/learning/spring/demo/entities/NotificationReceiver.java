package learning.spring.demo.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

@Data
@Entity
@Table(name = "NotificationReceivers")
public class NotificationReceiver {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "notification_id", columnDefinition = "bigint", length = 20, nullable = false)
    private Long notificationId;

    @Column(name = "user_id", columnDefinition = "bigint", length = 20, nullable = false)
    private Long userId;

    @Column(name = "device_id", columnDefinition = "bigint", length = 20, nullable = false)
    private Long deviceId;

    @Column(name = "org_id", columnDefinition = "bigint", length = 20, nullable = false)
    private Long orgId;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd/mm/yyyy h:m:s")
    @Column(name = "created_at", nullable = false, updatable = false)
    private Date createdAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Long notificationId) {
        this.notificationId = notificationId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    @PrePersist
    protected void onCreate() {
        createdAt = new Date();
    }
}
