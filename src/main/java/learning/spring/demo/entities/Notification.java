package learning.spring.demo.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

@Data
@Entity
@Table(name = "Notifications")
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name", nullable = false, columnDefinition = "varchar", length = 50)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "org_id", columnDefinition = "bigint", length = 20, nullable = false)
    private Long orgId;

    @Column(name = "event_id", columnDefinition = "bigint", length = 20, nullable = false)
    private Long eventId;

    @Column(name="deleted", columnDefinition = "boolean")
    private  boolean deleted = false;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd/mm/yyyy h:m:s")
    @Column(name = "created_at", nullable = false, updatable = false)
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd/mm/yyyy h:m:s")
    @Column(name = "updated_at")
    private Date updatedAt;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd/mm/yyyy h:m:s")
    @Column(name = "deleted_at")
    private Date deletedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    @PrePersist
    protected void onCreate() {
        createdAt = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updatedAt = new Date();
    }

    protected void markAsDeleted() {
        this.deletedAt = new Date();
    }
}
